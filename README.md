# Projeto: AutoAction

### Como usar?

```php
<?php

use AutoAction\Logs\GoogleCloud\AutoLogger;
use AutoAction\Logs\GoogleCloud\ConfigLogs;
use AutoAction\Logs\GoogleCloud\Logger;


require 'vendor/autoload.php';

// - - - - - - - - - - - - - - - - - - - - - - - - - -
//  Configuração
// - - - - - - - - - - - - - - - - - - - - - - - - - -

$credential = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'arquivo-de-credencial.json';
$projectId = 'id-do-projeto';
$namespaceId = 'seu-namespace';

$config = new ConfigLogs();
$config->setProjectId($projectId);
$config->setNamespaceId($namespaceId);
$config->setCredentials($credential);

// - - - - - - - - - - - - - - - - - - - - - - - - - -
//  Opção 1 - Logger
// - - - - - - - - - - - - - - - - - - - - - - - - - -
$logger = new Logger($config);
$logger->addSearchable(['pesquisa'=>'ok']);
$logger->addData(['example'=>'meu exemplo']);
$logger->log();

// - - - - - - - - - - - - - - - - - - - - - - - - - -
//  Opção 2 - AutoLogger
// - - - - - - - - - - - - - - - - - - - - - - - - - -
AutoLogger::setLogger(new Logger($config));
AutoLogger::save();
```
 ---

### Informações Docker

 Adicione no seu arquivo de hosts
 
 ```127.0.0.1   web.local```
 
 Normalmente este arquivo fica em: `/etc/hosts`
 
 Para iniciar o container rode o comando:
 
 `docker-compose up -d`
 
 ### Acessando a URL do projeto
 
 Adicione no seu navegador ou postman `http://web.local:8191`
 
 ### Configurando o Xdebug no PhpStorm
 
 Vá no menu de configuração `File > Settings... > Languagens & Frameworks > PHP > Debug`
 
 Na seção `Xdebug` no campo `Debug port:` coloque a porta `108191`
 
 Também é preciso mapear as pastas do container
 
 Vá no menu de configuração `File > Settings... > Languagens & Frameworks > PHP > Servers`
 
 ### Outros comandos Docker
 
 Inicia Docker: `docker-compose up -d`
 
 Encerrar Docker: `docker-compose down`
 
 Levantar o docker com rebuild do container: `docker-compose up -d --build`
 
 Entrar no shell do container: `docker exec -it autoaction-utils-phpfpm /bin/sh`
 
 Verificar os logs de acesso do nginx: `docker logs -tf autoaction-logs-web`
 
 