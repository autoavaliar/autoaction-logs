<?php

declare(strict_types=1);

namespace AutoAction\Logs\GoogleCloud;

use Exception;
use Throwable;

/**
 * Registro de AutoLog
 *
 * @package Mercury\Common\Util
 * @date    03/08/18 09:37
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class AutoLogger
{
    public static $isActive = true;
    public static $label = 'success';
    public static $data = [];
    public static $searchable = [];
    public static $logTracker = [];

    /**
     * @var Logger
     */
    private static $logger;

    public static function addTrack(array $data)
    {
        self::$isActive = true;
        self::$logTracker = array_merge(self::$logTracker, $data);
    }

    public static function getLogTracker(): array
    {
        return self::$logTracker;
    }

    public static function addData(array $data)
    {
        self::$isActive = true;
        self::$data = array_merge(self::$data, $data);
    }

    public static function disabled()
    {
        self::$isActive = false;
    }

    public static function addSearchable(array $searchable)
    {
        self::$isActive = true;
        self::$searchable = array_merge(self::$searchable, $searchable);
    }

    public static function addException(Throwable $e, $previous = null)
    {
        $excPrevious = null;

        if (!is_null($previous) && $previous instanceof Throwable) {
            $excPrevious = [
                'type' => 'exception',
                'message' => $previous->getMessage(),
                'code' => $previous->getCode(),
                'file' => $previous->getFile() . ':' . $e->getLine(),
            ];
        }

        $track[] = [
            'type' => 'exception',
            'message' => $e->getMessage(),
            'code' => $e->getCode(),
            'file' => $e->getFile() . ':' . $e->getLine(),
            'previous' => $excPrevious,
        ];
        self::addTrack($track);
    }

    public static function setFailure()
    {
        self::$label = 'error';
    }

    public static function setLogger(Logger $logger)
    {
        self::$logger = $logger;
    }

    public static function getLogger(): Logger
    {
        return self::$logger;
    }

    public static function save()
    {
        if (is_null(self::$logger)) {
            throw new Exception('setLogger not found!');
        }

        self::$logger->addData(['track' => self::getLogTracker()]);
        self::$logger->setLabel(self::$label);
        self::$logger->addData(self::$data);
        self::$logger->addSearchable(self::$searchable);
        self::$logger->log();
    }
}