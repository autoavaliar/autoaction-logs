<?php

declare(strict_types=1);

namespace AutoAction\Logs\GoogleCloud;

use AutoAction\Utils\Date;
use Exception;
use Google\Cloud\Datastore\DatastoreClient;
use stdClass;

/**
 * Armazenamento de logs Persistente no Google
 *
 * @package Core\App\Libs
 * @date    23/03/18 17:33
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class Logger
{
    /**
     * Dados para armazenar no log
     *
     * @var array
     */
    private $data = [];

    /**
     * Lista de chaves para remover o index
     *
     * @var array
     */
    private $excludeFromIndexes = ['data', 'context', 'ip', 'region-x'];

    private $context;

    /**
     * @var ConfigLogs
     */
    private $config;

    public function __construct(ConfigLogs $config)
    {
        $this->config = $config;
        $this->data['label'] = $config->getLabel();
        $this->data['data'] = [];
        $this->loadEnv();
        $this->loadDefault();
    }

    public function getConfig(): ConfigLogs
    {
        return $this->config;
    }

    /**
     * Carrega as credenciais
     */
    private function loadEnv()
    {
        putenv("GOOGLE_APPLICATION_CREDENTIALS={$this->config->getCredentials()}");
    }

    /**
     * Carrega registros padrão
     */
    private function loadDefault()
    {
        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        //  contexto do log
        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        $this->context = new stdClass();

        $this->context->user_agent = (isset($_SERVER['HTTP_USER_AGENT']))
            ? str_replace('/', '|', $_SERVER['HTTP_USER_AGENT'])
            : 'not_found';

        $this->context->request_method = (isset($_SERVER['REQUEST_METHOD']))
            ? $_SERVER['REQUEST_METHOD']
            : 'not_found';

        $this->context->request_uri = (isset($_SERVER['REQUEST_URI']))
            ? str_replace('/', '|', $_SERVER['REQUEST_URI'])
            : 'not_found';

        $this->context->server_name = (isset($_SERVER['SERVER_NAME'])) ? $_SERVER['SERVER_NAME'] : 'not_found';

        $this->context->header = getallheaders();
        $this->context->header = $this->transformeData($this->context->header);

        $regionX = '';

        if (isset($this->context->header['X-Appengine-Country'])) {
            $regionX = $this->context->header['X-Appengine-Country'];
        }

        if (isset($this->context->header['X-Appengine-Region'])) {
            $regionX .= ' / ' . $this->context->header['X-Appengine-Region'];
        }

        if (isset($this->context->header['X-Appengine-City'])) {
            $regionX .= ' / ' . $this->context->header['X-Appengine-City'];
        }

        if (isset($this->context->header['X-Appengine-Country'])) {
            $regionX .= ' / ' . $this->context->header['X-Appengine-Country'];
        }

        $this->data += [
            'created' => Date::nowWithMiliseconds(),
            'region-x' => $regionX,
            'context' => json_encode($this->context),
            'url' => $this->fullUrl(),
        ];

        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        // remove os base64
        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        $rawBody = file_get_contents('php://input');
        if (isset($rawBody['base64Images'])) {
            $rawBody['base64Images'] = count($rawBody['base64Images']) . ' imagem(s) enviadas.';
        }

        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        // remove conteúdos grandes de pubsub
        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        if (isset($rawBody['message']['data'])) {
            if (strlen($rawBody['message']['data']) > 1500) {
                $rawBody['message']['data'] = 'conteúdo removido';
            }
        }

        $this->addData(["get_raw_body" => $rawBody]);
    }

    private function urlOrigin($useForwardedHost = false)
    {
        $ssl = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on');
        $sp = strtolower($_SERVER['SERVER_PROTOCOL']);
        $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
        $port = $_SERVER['SERVER_PORT'];
        $port = ((!$ssl && $port == '80') || ($ssl && $port == '443')) ? '' : ':' . $port;
        $host = ($useForwardedHost && isset($_SERVER['HTTP_X_FORWARDED_HOST'])) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : null);
        $host = isset($host) ? $host : $_SERVER['SERVER_NAME'] . $port;
        return $protocol . '://' . $host;
    }

    private function fullUrl($useForwardedHost = false)
    {
        return $this->urlOrigin($useForwardedHost) . $_SERVER['REQUEST_URI'];
    }

    public function setLabel($label)
    {
        $this->data['label'] = $label;
    }

    public function getLabel()
    {
        return $this->data['label'];
    }

    /**
     * Salva o log no Datastorage
     *
     * @return string
     */
    private function save()
    {
        try {
            // - - - - - - - - - - - - - - - - - - - - - - - - - -
            //  inicializa
            // - - - - - - - - - - - - - - - - - - - - - - - - - -
            $dataStorage = new DatastoreClient(
                [
                    'projectId' => $this->config->getProjectId(),
                    'namespaceId' => $this->config->getNamespaceId()
                ]
            );

            // - - - - - - - - - - - - - - - - - - - - - - - - - -
            // prepara a gravação dos dados
            // - - - - - - - - - - - - - - - - - - - - - - - - - -
            $type = $this->config->getType() ?? 'default';
            if (strlen($this->context->request_uri) > 1 && is_null($this->config->getType())) {
                $type = str_replace('|', '_', $this->context->request_uri);
            }

            $key = $dataStorage->key($type);
            $log = $dataStorage->entity(
                $key,
                $this->data,
                ['excludeFromIndexes' => $this->excludeFromIndexes]
            );


            // - - - - - - - - - - - - - - - - - - - - - - - - - -
            //  gravar os dados
            // - - - - - - - - - - - - - - - - - - - - - - - - - -
            return $dataStorage->insert($log);
        } catch (Exception $e) {
            return $e->getCode() . ' ' . $e->getMessage();
        }
    }

    /**
     * Transforma os dados para armazenamento
     *
     * @param mixed $data Dados
     *
     * @return mixed
     */
    private function transformeData($data)
    {
        if (is_object($data) || is_array($data)) {
            return json_decode(json_encode($data, JSON_FORCE_OBJECT), true);
        } else {
            return [$data];
        }
    }

    /**
     * Adiciona um novo item ao log
     *
     * @param string $key Chave do log
     * @param string $value Dados do log
     */
    public function add($key, $value)
    {
        $this->data[$key] = $this->transformeData($value);
        $this->excludeFromIndexes = array_merge($this->excludeFromIndexes, [$key]);
    }

    /**
     * Adiciona no item ao dados do log
     *
     * @param mixed $data
     */
    public function addData($data)
    {
        $this->data['data'] = array_merge($this->data['data'], $this->transformeData($data));
    }

    /**
     * Adiciona um novo item pesquisavel ao log
     *
     * @param array $data dados
     *
     * @return Logger
     */
    public function addSearchable(array $data)
    {
        $this->data = array_merge($this->data, $data);
        return $this;
    }

    /**
     * Armazena o log no Datastore
     *
     * @param mixed $data
     * @param array $searchable
     *
     * @return string
     */
    public function log($data = [], array $searchable = [])
    {
        $this->addData($data);
        $this->addSearchable($searchable);
        return $this->save();
    }

    /**
     * Armazena uma exception
     *
     * @param Exception $e
     *
     * @return string
     */
    public function logException(Exception $e)
    {
        $this->setLabel('error');
        $log = [
            "error_code" => $e->getCode(),
            "error_message" => $e->getMessage(),
            "error_file" => $e->getFile() . ':' . $e->getLine()
        ];
        return $this->log($log);
    }
}
