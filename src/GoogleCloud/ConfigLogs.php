<?php

declare(strict_types=1);

namespace AutoAction\Logs\GoogleCloud;

use Exception;

/**
 * Arquivo de configuração para o Persistent GcpLogger
 *
 * @package Mercury\Common\Libs\GcpLogger
 * @date    08/05/18 18:13
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class ConfigLogs
{
    private $projectId;
    private $credentials;
    private $type;
    private $label = 'default';
    private $namespaceId;

    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;
    }

    public function setCredentials($credentials)
    {
        $this->credentials = $credentials;
    }

    public function setType(string $type)
    {
        $this->type = $type;
    }

    public function setLabel(string $label)
    {
        $this->label = $label;
    }

    public function setNamespaceId(string $namespaceId)
    {
        $this->namespaceId = $namespaceId;
    }

    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * Namespace é semelhante ao nome do banco de dados no relacional
     */
    public function getNamespaceId()
    {
        if (is_null($this->namespaceId) || empty($this->namespaceId)) {
            throw new Exception('NamespaceId not found!');
        }
        return $this->namespaceId;
    }

    /**
     * Type é semelhante ao nome de uma tabela no relacional
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Uma etiqueta, uma coluna no registro
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Path do arquivo de credenciais disponibilizado pelo Google
     */
    public function getCredentials()
    {
        return $this->credentials;
    }
}
